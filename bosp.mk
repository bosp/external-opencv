
ifdef CONFIG_EXTERNAL_OPENCV

# Targets provided by this project
.PHONY: opencv clean_opencv

# Add this to the "external" target
external: opencv
clean_external: clean_opencv

MODULE_DIR_OPENCV=external/partial/opencv

libs: libjpeg libv4l libtbb

extreq:
	@$(MODULE_DIR_OPENCV)/check_dependencies.sh

opencv: extreq setup libs $(BUILD_DIR)/lib/libopencv_core.so
$(BUILD_DIR)/lib/libopencv_core.so:
	@echo
	@echo "==== Building OpenCV library v2.3.1a ===="
	@[ -d $(MODULE_DIR_OPENCV)/build ] || mkdir $(MODULE_DIR_OPENCV)/build
	@cd $(MODULE_DIR_OPENCV)/build && 				\
		CXX=$(CXX) cmake $(CMAKE_COMMON_OPTIONS)	\
			-DBUILD_PYTHON_SUPPORT=ON 		\
			-DINSTALL_PYTHON_EXAMPLES=ON 		\
			-DWITH_V4L=ON 				\
			-DBUILD_EXAMPLES=ON 			\
			-DINSTALL_C_EXAMPLES=ON 		\
			-DWITH_TBB=ON 				\
			-DWITH_FFMPEG=OFF 			\
			-DWITH_CUDA=OFF 			\
			..
	@cd $(MODULE_DIR_OPENCV)/build && 				\
		CXX=$(CXX) make -j$(CPUS) install
	@echo

clean_opencv:
	@echo
	@echo "==== Clean-up OpenCV library v2.3.1a ===="
	@[ ! -f $(BUILD_DIR)/lib/libopencv_core.so ] || 	\
		rm -f $(BUILD_DIR)/lib/libopencv* && 		\
		rm -rf $(BUILD_DIR)/include/opencv* && 		\
		rm -rf $(BUILD_DIR)/share/OpenCV && 		\
		rm -rf $(BUILD_DIR)/share/opencv
	@[ ! -d $(MODULE_DIR_OPENCV)/build ] || 			\
		rm -rf $(MODULE_DIR_OPENCV)/build
	@echo

else # CONFIG_EXTERNAL_OPENCV

opencv:
	$(warning external/log4cpp module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EXTERNAL_OPENCV
