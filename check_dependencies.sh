#!/bin/bash


BOSP_MISSING_LIBS=0

function checkLibrary() {
	echo -n "Checking for $2... "
	pkg-config --list-all | grep $2 >/dev/null 2>&1
	if [ $? -eq 1 ]; then
		echo "FAILED!"
		echo "   Missing required $1 (development) library"
		BOSP_MISSING_LIBS=1
	else
		echo "OK!"
	fi
}

echo
echo "*** Checking external OpenCV dependencies ***"
echo

checkLibrary "GTK+2" "gtk+-2.0"
checkLibrary "PNG" "libpng"
checkLibrary "GStreamer" "gstreamer-0.10"
checkLibrary "GStreamer C++ Bindings" "gstreamermm-0.10"
checkLibrary "GStreamer Base" "gstreamer-base-0.10"
checkLibrary "GStreamer App" "gstreamer-app-0.10"
checkLibrary "GStreamer Video" "gstreamer-video-0.10"
#checkLibrary "Vorbis"		"libvorbis"
#CheckLibrary "TheoraEnc"	"libtheoraenc"
#CheckLibrary "TheoraDec"	"libtheoradec"
#CheckLibrary "USB"		"libusb"


if [ $BOSP_MISSING_LIBS -ne 0 ]; then
	echo
	echo "
The building of the OpenCV library, which is functional complete and efficient
for the BOSP usage purposes, requires a set of libraries which unfortunately
are not (yet) provided by the BOSP building system itself.

Please check the previous checking-list and install in your system all
libraries which test failed. You are required to install both the library and
its development files (i.e. headers).

On an Ubuntu 14.10 system this command should be enought to install required
additional packages:

$ sudo aptitude install libgstreamer0.10-dev libgstreamermm-0.10-dev libgtk2.0-dev



"
fi

exit $BOSP_MISSING_LIBS

